#!/usr/bin/env python3
import logging
import sys
import re
import webbrowser

from collections import defaultdict
from decimal import Decimal
from http.server import HTTPServer, BaseHTTPRequestHandler

import flair
import torch

from parser import load_classifier, name_to_cat, total_to_hledger
from tagger import (ModelNotFoundException,
                    full_to_entries, full_spans_to_entry_spans,
                    entry_span_to_record, split_full_text)


_LOGGER = logging.getLogger(__name__)


_TOK_START = re.compile(r'\b')


class Receipt:

    def __init__(self, inp):
        with open(inp) as infile:
            full_text = infile.read()
        _LOGGER.debug('Raw:\n%s', full_text)

        self._error = None

        try:
            self._full = split_full_text(full_text)
            spans = full_to_entries(full_text, is_interactive=False)
            self._entries = full_spans_to_entry_spans(spans)
            self._records = [entry_span_to_record(e) for e in self._entries]
        except ModelNotFoundException:
            self._error = "Splitter model not found"

        self._classifier = load_classifier()
        self._cats = [self._classify(*r) for r in self._records]

    def _classify(self, name, cost):
        label, _ = name_to_cat(name, cost, self._classifier)
        return label or ""

    @property
    def lines(self):
        return self._full

    @property
    def entries(self):
        return self._entries[:]

    @property
    def cats(self):
        return self._cats[:]

    @property
    def error(self):
        return self._error

    @property
    def records(self):
        return self._records[:]


class Visualizer(BaseHTTPRequestHandler):

    def do_error(self):
        self.wfile.write(b'<html><head><meta charset="utf-8"></head><body>')
        self.wfile.write(b'<p bgcolor="#f88">Error:')
        self.wfile.write(self.server.receipt.error.encode())
        self.wfile.write(b"</p>")
        self.wfile.write(b"</body></html>")

    def do_GET(self):
        if self.path == '/':
            self.show_parsed()
        elif self.path == 'favicon.ico':
            self.send_response(200)
            self.send_header('Content-Type', 'image/x-icon')
            self.send_header('Content-Length', 0)
            self.end_headers()
            return
        elif self.path.startswith('/confirm'):
            self.confirm()
        print("PATH:", self.path)

    def confirm(self):
        self.send_response(200)
        self.end_headers()
        receipt = self.server.receipt
        self.wfile.write(b'<html><head><meta charset="utf-8"></head><body>')
        self.wfile.write(b'<div style="width:60%">')

        totals = defaultdict(Decimal)
        for c, r in zip(self.server.receipt.cats, self.server.receipt.records):
            totals[c] += r[1]

        self.wfile.write(b'<pre>')
        for total in totals.items():
            if total[1] != 0:
                self.wfile.write(total_to_hledger(total).encode())
                self.wfile.write(b'<br>')
        self.wfile.write(b'</pre>')

        self.wfile.write(b'</div>')
        self.wfile.write(b'</body></html>')

    def show_parsed(self):
        self.send_response(200)
        self.end_headers()
        receipt = self.server.receipt
        if receipt.error is not None:
            return self.do_error()

        self.wfile.write(b'<html><head><meta charset="utf-8"></head><body>')
        self.wfile.write(b'<form style="width:60%" action="confirm">')
        entries = receipt.entries
        cats = receipt.cats
        records = receipt.records
        for line in receipt.lines:
            labels = line.get_labels()
            if labels:
                label = line.get_labels()[0].value
            else:
                label = 'O'
            if label == 'B-entry':
                self.write_entry(entries.pop(0), cats.pop(0), records.pop(0))
            elif label == 'O':
                self.wfile.write(line.text.encode())
                self.wfile.write(b'<br>')
        self.wfile.write(b'''
<div style="position:fixed;top:30px;right:30px;padding:20px;border: 1px solid black">
        <input type="submit" value="Confirm">
</div>''')
        self.wfile.write(b'</form>')
        self.wfile.write(b"</body></html>")

    def write_entry(self, entry, cat, record):
        _, cost = record
        self.wfile.write(b'<div style="border:1px solid blue; display:grid; grid-template-columns: 15fr 2fr 1fr">')
        text = entry.text
        in_span = False

        self.wfile.write(b'<div>')
        for token in entry.tokens:
            print(f"XXX <{token}>({type(token)})")
            split_res = _TOK_START.split(text, 1)
            if len(split_res) == 2:
                spaces, text = split_res
            else:
                spaces = ""
            labels = token.get_labels()
            if labels:
                tok_label = labels[0].value
            else:
                tok_label = 'O'
            if in_span and tok_label[0] != 'I':
                self.wfile.write(b'</span>')
                in_span = False
            self.wfile.write(spaces.replace("\n", "<br>").encode())
            if tok_label[0] == 'B':
                in_span = True
                tag = tok_label[2:]
                color = {'name': b'#ff8',
                         'cost': b'#88f'}[tag]
                self.wfile.write(b'<span style="background:')
                self.wfile.write(color)
                self.wfile.write(b'">')
            self.wfile.write(token.text.encode())
            text = text[len(token.text):]
        if in_span:
            self.wfile.write(b'</span>')
        self.wfile.write(b'</div>')

        self.wfile.write(b'<div style="border:1px solid yellow">')
        self.wfile.write(cat.encode())
        self.wfile.write(b'</div>')

        self.wfile.write(b'<div style="border:1px solid green">')
        self.wfile.write(str(cost).encode())
        self.wfile.write(b'</div>')

        self.wfile.write(b'</div>')

def main(inp):
    receipt = Receipt(inp)

    server_address = ('', 0)
    server = HTTPServer(server_address, Visualizer)
    server.receipt = receipt
    server.entries = []
    port = server.server_address[1]
    webbrowser.open(f"http://localhost:{port}")
    server.serve_forever()


if __name__ == '__main__':
    flair.device = torch.device('cuda:0')
    log_format = "%(asctime)-8s:%(levelname)5s:%(name)s: %(message)s"
    logging.basicConfig(stream=sys.stderr,
                        level=logging.DEBUG,
                        format=log_format,
                        datefmt="%H:%M:%S")
    main(sys.argv[1])
