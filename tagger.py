#!/usr/bin/env python
import os

from decimal import Decimal
from pathlib import Path
from typing import List

from flair.data import Sentence, Token
from flair.models import SequenceTagger
from flair.tokenization import Tokenizer

from utils import maybe_save, interactive_verify


_MODEL = Path(__file__).parent / 'tag_model'
_MODEL.mkdir(exist_ok=True)

_TAGS_DIR = Path(__file__).parent / 'data' / 'tags'
_SPLITS_DIR = Path(__file__).parent / 'data' / 'splits'

_SPLITS_COLUMN_SEPARATOR = '\tXXX\t'

_TOKENIZER_RETURNS_TOKENS = False

class NewlineTokenizer(Tokenizer):
    """Tokenizer based on split."""

    def tokenize(self, text: str) -> List[str]:
        return NewlineTokenizer.run_tokenize(text)

    @staticmethod
    def run_tokenize(text: str) -> List[str]:
        tokens: List[Token] = []
        word = ""
        index = -1
        for index, char in enumerate(text):
            if char == '\n':
                if len(word) > 0:
                    start_position = index - len(word)
                    if _TOKENIZER_RETURNS_TOKENS:
                        tokens.append(Token(text=word,
                                            start_position=start_position,
                                            whitespace_after=True))
                    else:
                        tokens.append(word)
                word = ""
            else:
                word += char
        # increment for last token in sentence if not followed by whitespace
        index += 1
        if len(word) > 0:
            start_position = index - len(word)
            if _TOKENIZER_RETURNS_TOKENS:
                tokens.append(Token(text=word,
                                    start_position=start_position,
                                    whitespace_after=False))
            else:
                tokens.append(word)
        return tokens


class SplitTokenizer(Tokenizer):
    """Tokenizer based on split."""

    def tokenize(self, text: str) -> List[str]:
        return SplitTokenizer.run_tokenize(text)

    @staticmethod
    def run_tokenize(text: str) -> List[str]:
        tokens: List[Token] = []
        word = ""
        index = -1
        for index, char in enumerate(text):
            if char.isspace():
                if len(word) > 0:
                    start_position = index - len(word)
                    if _TOKENIZER_RETURNS_TOKENS:
                        tokens.append(Token(text=word,
                                            start_position=start_position,
                                            whitespace_after=True))
                    else:
                        tokens.append(word)
                word = ""
            else:
                word += char
        # increment for last token in sentence if not followed by whitespace
        index += 1
        if len(word) > 0:
            start_position = index - len(word)
            if _TOKENIZER_RETURNS_TOKENS:
                tokens.append(Token(text=word,
                                    start_position=start_position,
                                    whitespace_after=False))
            else:
                tokens.append(word)

        return tokens


def tags_to_lines(tags):
    result = []
    for sent in tags:
        result.extend(f"{tag}\t{token}" for token, tag in sent)
        result.append("")
    return result


class ModelNotFoundException(Exception):

    def __init__(self, model_type):
        super().__init__(f"{model_type.capitalize()} model not found")


def sentence_to_spans(sentence, separator):
    spans = []
    for token in sentence:
        label = ([l.value for l in token.labels] + ['O'])[0]
        if label[0] == 'B':
            spans.append([token.text, label[2:]])
        elif label[0] == 'I':
            spans[-1][0] += separator + token.text
    return spans


def split_full_text(full_text):
    splitter_dir = _MODEL / 'split'
    if not splitter_dir.exists():
        raise ModelNotFoundException("splitter")

    splitter_path = str(splitter_dir / 'final-model.pt')
    splitter = SequenceTagger.load(splitter_path)
    sentence = Sentence(full_text,
                        use_tokenizer=NewlineTokenizer())
    splitter.predict(sentence, force_token_predictions=True)
    return sentence


def full_to_entries(full_text, is_interactive=True):
    try:
        sentence = split_full_text(full_text)
    except ModelNotFoundException as ex:
        print(ex)
        from split_trainer import main as split_trainer_main
        from tempfile import mkstemp
        fd, raw = mkstemp(text=True)
        try:
            with os.fdopen(fd, "w") as data:
                print(full_text, file=data)
            data.close()
            split_trainer_main([raw])
        finally:
            os.unlink(raw)
        sentence = split_full_text(full_text)

    if is_interactive:
        span_tags = []
        for token in sentence:
            label = ([l.value for l in token.labels] + ['O'])[0]
            span_tags.append((token.text, label))
        span_tags = interactive_verify(span_tags, labels=['O', 'B-entry', 'I-entry'])
        for token, updated in zip(sentence.tokens, span_tags):
            token.set_label("ner", updated[1])
        maybe_save(tuple(span_tags), _SPLITS_DIR,
                   column_separator=_SPLITS_COLUMN_SEPARATOR)
    return sentence_to_spans(sentence, "\n")


def cost_to_decimal(cost):
    if ',' in cost or '.' in cost:
        # if cost contains both spaces and either colon or dot, discard spaces
        cost = cost.replace(" ", "")
    else:
        # otherwise treat space as a colon
        cost = cost.replace(" ", ".")
    cost = cost.replace(",", ".")
    return Decimal(cost)


class Entry:

    _tagger = None

    def __init__(self, text, tagger=None):
        self._text = text
        self._sentence = Sentence(text,
                                  use_tokenizer=SplitTokenizer())

        if tagger is None:
            if self._tagger is None:
                type(self)._tagger = type(self).load_tagger()
            tagger = self._tagger
        tagger.predict(self._sentence, force_token_predictions=True)

    @classmethod
    def load_tagger(cls):
        tagger_dir = _MODEL / 'tag'
        if not tagger_dir.exists():
            raise ModelNotFoundException("tagger")

        tagger_path = str(tagger_dir / 'final-model.pt')
        return SequenceTagger.load(tagger_path)

    @property
    def text(self):
        return self._text

    @property
    def sentence(self):
        return self._sentence

    @property
    def tokens(self):
        return self._sentence.tokens

    def as_dict(self):
        spans = self._sentence.get_spans('ner')
        return {span.labels[0].value: span.text for span in spans}

    @property
    def name(self):
        for span in self._sentence.get_spans('ner'):
            if span.labels[0].value == 'name':
                return span.text

    @property
    def cost(self):
        for span in self._sentence.get_spans('ner'):
            if span.labels[0].value == 'cost':
                return cost_to_decimal(span.text)


def make_entry(text, tagger, is_interactive=True):
    entry = Entry(text, tagger)
    if is_interactive:
        span_tags = []
        for token in entry.tokens:
            label = ([l.value for l in token.labels] + ['O'])[0]
            span_tags.append((token.text, label))
        updated_tags = interactive_verify(span_tags,
                                          labels=['O',
                                                  'B-name', 'I-name',
                                                  'B-cost', 'I-cost'])
        for token, updated in zip(entry.tokens, updated_tags):
            token.set_label("ner", updated[1])
    return entry


def entries_to_records(entries, is_interactive=True):
    try:
        tagger = Entry.load_tagger()
    except ModelNotFoundException as ex:
        print(ex)
        from tag_trainer import main as tag_trainer_main
        from tempfile import mkstemp
        fd, raw = mkstemp(text=True)
        try:
            with os.fdopen(fd, "w") as data:
                for entry in entries:
                    for token in entry.text:
                        print(token,file=data)
                    print(file=data)
            data.close()
            tag_trainer_main([raw])
        finally:
            os.unlink(raw)
        tagger = Entry.load_tagger()

    entry_objects = [make_entry(entry[0], tagger, is_interactive)
                     for entry in entries]

    if is_interactive:
        predicted = [[(token.text, token.get_labels()[0].value)
                      for token in entry.tokens]
                     for entry in entry_objects]

        maybe_save(predicted, _TAGS_DIR)
    return [entry_span_to_record(obj) for obj in entry_objects]

def entry_span_to_record(entry_span):
    as_dict = {k:v for v,k in sentence_to_spans(entry_span.sentence, " ")}
    return (as_dict["name"], cost_to_decimal(as_dict.get("cost", "0")))

def full_spans_to_entry_spans(spans):
    tagger = Entry.load_tagger()

    return [make_entry(span[0], tagger, False) for span in spans]
