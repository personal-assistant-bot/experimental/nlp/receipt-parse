#!/usr/bin/env python
import argparse
import logging
import shutil
import sys

from pathlib import Path

from flair.datasets import ClassificationCorpus
from flair.embeddings import DocumentRNNEmbeddings
from flair.models import TextClassifier
from flair.trainers import ModelTrainer


from n_utils import make_embeddings, make_vocab, train_lm_model


_LOGGER = logging.getLogger(__name__)


_MODEL = Path(__file__).parent / 'model'
_MODEL.mkdir(exist_ok=True)


def make_corpus(source_files=None):
    with open(_MODEL / 'corpus.txt', 'w') as corpus:
        Path('data/processed').mkdir(exist_ok=True)
        for f in Path('data/processed').iterdir():
            with open(f) as labeled:
                for line in labeled:
                    l = line.strip()
                    if not l:
                        continue
                    name, _ = l.rsplit(None,1)
                    print(name, file=corpus)
        if source_files is not None:
            for f in source_files:
                with open(f) as raw:
                    data = raw.read()
                    samples = data.split("\n\n")
                for sample in samples:
                    for l in sample.split('\n'):
                        if not l:
                            continue
                        print(l, file=corpus)
    (_MODEL / 'corpus/train').mkdir(parents=True, exist_ok=True)
    shutil.copyfile(_MODEL / 'corpus.txt', _MODEL / 'corpus/train/train.txt')
    shutil.copyfile(_MODEL / 'corpus.txt', _MODEL / 'corpus/test.txt')
    shutil.copyfile(_MODEL / 'corpus.txt', _MODEL / 'corpus/valid.txt')
    return _MODEL / 'corpus'


def make_cls_corpus(source_files=None):
    (_MODEL / 'labels').mkdir(exist_ok=True)
    with open(_MODEL / 'labels/train.txt', 'w') as labels:
        for f in Path('data/processed').iterdir():
            with open(f) as labeled:
                for line in labeled:
                    l = line.strip()
                    if not l:
                        continue
                    name, label = l.rsplit(None,1)
                    print(f'__label__{label}', name, file=labels)

        if source_files is not None:
            for f in source_files:
                with open(f) as raw:
                    for l in raw:
                        if not l:
                            continue
                        print("__label__<unsorted>", l)
                        print("__label__<unsorted>", l, file=labels)

    shutil.copyfile(_MODEL / 'labels/train.txt', _MODEL / 'labels/test.txt')
    shutil.copyfile(_MODEL / 'labels/train.txt', _MODEL / 'labels/dev.txt')
    return _MODEL / 'labels'


def train_cls_model(corpus_path, word_embeddings, model, batch_size=32):
    corpus = ClassificationCorpus(corpus_path)
    label_dict = corpus.make_label_dictionary("class")
    document_embeddings = DocumentRNNEmbeddings([word_embeddings],
                                                hidden_size=512,
                                                reproject_words=True,
                                                reproject_words_dimension=256)


    if (model / 'final-model.pt').exists():
        classifier = TextClassifier.load(model / 'final-model.pt')
        params = {"max_epochs": 10}
    else:
        classifier = TextClassifier(document_embeddings,
                                    label_dictionary=label_dict,
                                    label_type="class")
        params = {"max_epochs": 150,
                  "save_model_each_k_epochs": 25}
    trainer = ModelTrainer(classifier, corpus)
    trainer.train(model,
                  anneal_with_restarts=True,
                  learning_rate=0.1,
                  mini_batch_size=batch_size,
                  anneal_factor=0.5,
                  patience=5,
                  **params)


def main(args, source_files=None):
    corpus = make_corpus(source_files)
    mappings, changed = make_vocab(_MODEL)
    if changed:
        if (_MODEL / 'forward').exists():
            shutil.rmtree(_MODEL / 'forward')
        if (_MODEL / 'backward').exists():
            shutil.rmtree(_MODEL / 'backward')
        if (_MODEL / 'cls').exists():
            shutil.rmtree(_MODEL / 'cls')

    train_lm_model(corpus, mappings, _MODEL / 'forward', forward=True)
    train_lm_model(corpus, mappings, _MODEL / 'backward', forward=False)

    if args.lm_only:
        return

    cls_corpus = make_cls_corpus(source_files)
    embeddings = make_embeddings(_MODEL)
    train_cls_model(cls_corpus, embeddings, _MODEL / 'cls')


if __name__ == '__main__':
    log_format = "%(asctime)-8s:%(levelname)5s:%(name)s: %(message)s"
    logging.basicConfig(stream=sys.stderr,
                        level=logging.DEBUG,
                        format=log_format,
                        datefmt="%H:%M:%S")
    parser = argparse.ArgumentParser()
    parser.add_argument("--lm-only", action='store_true')

    main(parser.parse_args())
