import logging


_LOGGER = logging.getLogger(__name__)


def ask_ynq():
    is_ok = input("Is this ok? [Ynq] ")
    if not is_ok:
        is_ok = 'y'
    while is_ok not in 'ynq':
        is_ok = input("Please enter y, n, q or nothing ")
        if not is_ok:
            is_ok = 'y'
    return is_ok


def ask_fix():
    return input("Change anything? (press enter if OK) ").strip()


def parse_one_number(num, low, high):
    """
    This function parses single number and verifies it falls within given range.

    >>> parse_one_number('1', 1, 10)
    1
    >>> parse_one_number('100', 1, 10)
    Traceback (most recent call last):
        ...
    ValueError: 100 is not in range 1-10
    >>> parse_one_number('x', 1, 10)
    Traceback (most recent call last):
        ...
    ValueError: 'x' is not a number
    """
    if not num.isdigit():
        raise ValueError(f"'{num}' is not a number")
    value = int(num)
    if not low <= value <= high:
        raise ValueError(f"{value} is not in range {low}-{high}")
    return int(num)


def parse_num_range(spec, low, high):
    """
    This function parses single number or range of numbers.
    It returns the first and last numbers of range.
    >>> parse_num_range('1', 1, 10)
    (1, 1)
    >>> parse_num_range('1-3', 1, 10)
    (1, 3)

    Start or end could be ommitted:
    >>> parse_num_range('5-', 1, 10)
    (5, 10)
    >>> parse_num_range('-5', 1, 10)
    (1, 5)

    A special case is '*' which means full range:
    >>> parse_num_range('*', 1, 10)
    (1, 10)
    """
    if spec == '*':
        return low, high
    if '-' in spec:
        start, end = spec.split('-')
        if start:
            start = parse_one_number(start, low, high)
        else:
            start = low
        if end:
            end = parse_one_number(end, low, high)
        else:
            end = high
        return start, end
    else:
        num = parse_one_number(spec, low, high)
        return num, num


def parse_num_spec(spec, low, high):
    """
    This function parses the list of numbers and returns it.

    >>> parse_num_spec('1', 1, 10)
    [1]
    >>> parse_num_spec('100', 1, 10)
    Traceback (most recent call last):
        ...
    ValueError: 100 is not in range 1-10
    >>> parse_num_spec('x', 1, 10)
    Traceback (most recent call last):
        ...
    ValueError: 'x' is not a number

    It can also parse comma-separated list of numbers
    >>> parse_num_spec('1,3,5', 1, 10)
    [1, 3, 5]
    >>> parse_num_spec('1,x,5', 1, 10)
    Traceback (most recent call last):
        ...
    ValueError: 'x' is not a number
    >>> parse_num_spec('1,100,5', 1, 10)
    Traceback (most recent call last):
        ...
    ValueError: 100 is not in range 1-10

    Ranges can be specified using dash
    >>> parse_num_spec('1-3', 1, 10)
    [1, 2, 3]

    Result is sorted and repeated numbers are discarded:
    >>> parse_num_spec('1,1', 1, 10)
    [1]
    >>> parse_num_spec('2,1', 1, 10)
    [1, 2]
    """
    result = set()
    for segment in spec.split(','):
        start, end = parse_num_range(segment, low, high)
        result |= set(range(start, end+1))
    return list(sorted(result))


def interactive_verify(cats, labels=None):
    while True:
        for i, (name, label) in enumerate(cats):
            print(f"{i+1:2d} {name}\t{label}")
        incorrect = ask_fix()
        if not incorrect:
            break
        while True:
            if incorrect:
                incorrect, *rest = incorrect.split(None, 1)
                newcat = rest[0] if rest else None

                try:
                    incorrect_numbers = parse_num_spec(incorrect, 1, len(cats))
                    break
                except ValueError as v:
                    print(v)

            incorrect = input(f"Which number to fix? [1-{len(cats)}]: ").strip()
        for n in incorrect_numbers:
            name, label = cats[n-1]
            if newcat is None:
                this_newcat = input(f"Enter the correct category for \"{name}\" (current: \"{label}\"): ")
            else:
                this_newcat = newcat
            if labels is not None and this_newcat not in labels:
                print(f"Label '{this_newcat}' is not valid")
                print("Valid labels:")
                for l in labels:
                    print("-", l)
                break
            cats[n-1] = (name, this_newcat)
    return cats


def write_one_record(record, out, sep):
    for name, label in record:
        print(name, label, sep=sep, file=out)


def maybe_save(cats, path, column_separator='\t'):
    should_save = input("Save processed data? [Y/n] ")
    if should_save in ('', 'y', 'Y'):
        do_save(cats, path, column_separator)

def do_save(cats, path, column_separator='\t'):
    if not path.exists():
        path.mkdir(parents=True)
    i = 1
    while (path / f'input{i}.txt').exists():
        i += 1
    with open(path / f'input{i}.txt', 'w') as processed:
        if isinstance(cats, list):
            for record in cats:
                write_one_record(record, processed, column_separator)
                print(file=processed)
        else:
            write_one_record(cats, processed, column_separator)
