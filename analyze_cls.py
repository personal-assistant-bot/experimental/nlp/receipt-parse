#!/usr/bin/env python

from pathlib import Path

from flair.data import Sentence
from flair.models import TextClassifier


_MODEL = Path(__file__).parent / 'model'


def main():
    classifier = TextClassifier.load(str(_MODEL / 'cls/final-model.pt'))
    with open(_MODEL / 'labels/test.txt') as labels:
        for line in labels:
            l, name = line.strip().split(None, 1)
            actual_label = l[len("__label__"):]
            sentence = Sentence(name)
            classifier.predict(sentence)
            if not sentence.labels:
                print("NONE", actual_label, name, sep="\t")
            else:
                predicted_label = sentence.labels[0].value
                if predicted_label != actual_label:
                    print(predicted_label, actual_label, name, sep="\t")


if __name__ == '__main__':
    main()
