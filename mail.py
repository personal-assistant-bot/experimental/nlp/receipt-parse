#!/usr/bin/env python3
import logging
import mailbox
import sys

from collections import defaultdict
from email.header import decode_header

from html2text import HTML2Text


_LOGGER = logging.getLogger(__name__)

_INBOX = "~/.mbsync/gmail/Inbox"

_sources = set([
    "<noreply@1-ofd.ru>",
    "<noreply@ofd.ru>",
    "payment@iboxmpos.com",
    "<ofdreceipt@beeline.ru>",
    "<robot@konturcheck.ru>",
    "<noreply@chek.pofd.ru>",
    "<no-reply@pay.coolclever.ru>",
    "<noreply@utkonos.ru>",
    "<echeck@1-ofd.ru>",
])


def get_text(msg):
    parts = defaultdict(list)
    for part in msg.walk():
        if part.get_content_maintype() == 'text':
            parts[part.get_content_subtype()].append(part)
    if 'html' in parts:
        return parts['html'][0]
    else:
        return parts['plain'][0]

count = 0

def process_msg(msg, addr):
    body = get_text(msg)
    if body is None:
        print("No body")
        return
    payload = body.get_payload(decode=True)
    data = payload.decode(body.get_content_charset('utf-8'))
    is_html = body.get_content_subtype() == 'html' or addr == "payment@iboxmpos.com"
    if is_html:
        h2t = HTML2Text()
        h2t.ignore_links = True
        h2t.ignore_tables = True
        h2t.ignore_images = True
        h2t.ignore_emphasis = True
        result = h2t.handle(data)
    else:
        result = data
    global count
    filename = f"/tmp/receipt{count}.txt"
    with open(filename, "w") as out:
        print(result, file=out)
    print(filename)
    count += 1


def main():
    mdir = mailbox.Maildir(_INBOX)
    for message in mdir:
        if 'S' in message.get_flags():
            continue
        data, enc = decode_header(message['From'])[-1]
        if isinstance(data, str):
            addr = data
        elif enc is None:
            addr = data.decode()
        else:
            addr = data.decode(enc)
        _LOGGER.info("addr = %s", addr)
        if addr.split()[-1] not in _sources:
            continue
        process_msg(message, addr)


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)
    main()
