#!/usr/bin/env python
import logging
import os
import subprocess
import sys

from collections import defaultdict
from decimal import Decimal
from pathlib import Path

from flair.data import Sentence
from flair.models import TextClassifier

from utils import interactive_verify, maybe_save
from tagger import full_to_entries, entries_to_records

_LOGGER = logging.getLogger(__name__)


_MODEL = Path(__file__).parent / 'model'

_PROCESSED_DIR = Path(__file__).parent / 'data' / 'processed'


_cat_changes = {
    "быт.товары": ("expenses:дом:бытовые товары",),
    "гигиена": ("expenses:здоровье", "details:гигиена"),
    "лекарства": ("expenses:здоровье", "details:лекарства"),
}

def load_classifier():
    if (_MODEL / 'cls/final-model.pt').exists():
        return TextClassifier.load(str(_MODEL / 'cls/final-model.pt'))


def name_to_cat(name, price, classifier):
    if price is not None and price <= 0:
        return None, None
    sentence = Sentence(name)
    classifier.predict(sentence)
    if sentence.labels:
        return sentence.labels[0].value, sentence.labels
    return None, None


def train_classifier(records):
    from trainer import main as trainer_main
    from tempfile import mkstemp

    class TrainerArgs:
        lm_only = False

    fd, raw = mkstemp(text=True)
    try:
        with os.fdopen(fd, "w") as data:
            for (name,_) in records:
                print(name,file=data)
        data.close()
        trainer_main(TrainerArgs(), [raw])
    finally:
        os.unlink(raw)
    return load_classifier()


def records_to_categories(records, is_interactive=True):
    cats = []

    classifier = load_classifier()
    if classifier is None:
        classifier = train_classifier(records)
    for name, price in records:
        label, labels = name_to_cat(name, price, classifier)
        if label is None:
            continue
        cats.append((name, label))
        print(name, labels)

    if is_interactive:
        cats = interactive_verify(cats)
    if cats is None:
        return

    if is_interactive:
        maybe_save(tuple(cats), _PROCESSED_DIR)
    return dict(cats)


def total_to_hledger(total):
    cat, tot = total
    if cat in _cat_changes:
        ncat, *tags = _cat_changes[cat]
        tags_list = ", ".join(tags)
        line = f" {ncat}  {tot}"
        if tags_list:
            line += " ; " + tags_list
    else:
         line = f" expenses:продукты  {tot}  ; groceries:{cat}"
    return line

def main(inp):
    with open(inp) as infile:
        full_text = infile.read()
    _LOGGER.debug('Raw:\n%s', full_text)

    entries = full_to_entries(full_text)
    records = entries_to_records(entries)
    assert records is not None
    cats = records_to_categories(records)

    totals = defaultdict(Decimal)
    for name, price in records:
        if price > 0 and cats[name] != 'включено':
            totals[cats[name]] += price

    output = [total_to_hledger(total) for total in totals.items()]
    print(*output, sep="\n")
    subprocess.run("xclip", input="\n".join(output).encode())


if __name__ == '__main__':
    log_format = "%(asctime)-8s:%(levelname)5s:%(name)s: %(message)s"
    logging.basicConfig(stream=sys.stderr,
                        level=logging.DEBUG,
                        format=log_format,
                        datefmt="%H:%M:%S")
    main(sys.argv[1])
