#!/usr/bin/env python

from pathlib import Path

from flair.data import Sentence
from flair.models import SequenceTagger

from tagger import NewlineTokenizer


_MODEL = Path(__file__).parent / 'tag_model'
_DATA = Path(__file__).parent / 'data' / 'splits'


def main():
    splitter = SequenceTagger.load(str(_MODEL / 'split/final-model.pt'))
    for f in _DATA.iterdir():
        labeled = []
        with open(f) as data:
            for l in data.readlines():
                if "\tXXX\t" not in l:
                    continue
                labeled.append(l.rstrip().split("\tXXX\t"))
        full_text = '\n'.join(l[0] for l in labeled)
        sentence = Sentence(full_text,
                            use_tokenizer=NewlineTokenizer())
        splitter.predict(sentence, force_token_predictions=True)
        wrong = []
        for i, (token, my) in enumerate(zip(sentence, labeled)):
            label = ([l.value for l in token.labels] + ['O'])[0]
            if label != my[1]:
                wrong.append((i+1, *my, token))

        if wrong:
            print("=" * 20)
            print(f)
            for w in wrong:
                print(*w, sep="\t")


if __name__ == '__main__':
    main()
