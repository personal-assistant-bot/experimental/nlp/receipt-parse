import unittest


from utils import parse_num_spec


class NumberSpecParseTest(unittest.TestCase):

    def test_parse_incorrect(self):
        cases = {
            '': None,
            'x': None,
            '1a': None,
            'x1': None,
            '1x1': None,
        }
