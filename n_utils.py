import logging
import pickle

from collections import Counter
from pathlib import Path
from typing import List

from flair.data import Dictionary
from flair.embeddings import FlairEmbeddings, StackedEmbeddings, TokenEmbeddings
from flair.models import LanguageModel
from flair.trainers.language_model_trainer import LanguageModelTrainer, TextCorpus


_LOGGER = logging.getLogger(__name__)


def make_vocab(model_path, corpus_name='corpus'):
    char_dictionary = Dictionary()
    counter = Counter()
    with open(model_path / f'{corpus_name}.txt', 'r', encoding='utf-8') as f:
        for line in f:
            counter.update(list(line))

    total_count = 0
    for letter, count in counter.most_common():
        total_count += count

    result = model_path / 'flair_char_mappings.pickle'
    if result.exists():
        with open(result, 'rb') as f:
            old = pickle.load(f)
    else:
        old = {'item2idx':{}}

    total = 0
    idx = 0
    for letter, count in counter.most_common():
        total += count
        percentile = (total / total_count)
        char_dictionary.add_item(letter)
        idx += 1
        _LOGGER.info('%d\t%s\t%7d\t%7d\t%f' % (idx, letter, count, total, percentile))

    _LOGGER.info("%s", char_dictionary.item2idx)
    for letter, _ in counter.most_common():
        if letter.encode() not in old['item2idx']:
            _LOGGER.info("Vocabulary changed: [%s] not in old vocabulary", letter)
            break
    else:
        _LOGGER.info("Vocabulary unchanged")
        return result, False

    with open(result, 'wb') as f:
        mappings = {'idx2item': char_dictionary.idx2item,
                    'item2idx': char_dictionary.item2idx}
        pickle.dump(mappings, f)

    return result, True


def train_lm_model(corpus, mappings, model,
                   hidden_size=128,
                   forward=True,
                   batch_size=256):
    dictionary: Dictionary = Dictionary.load_from_file(mappings)

    corpus = TextCorpus(corpus,
                        dictionary,
                        forward,
                        character_level=True)

    if (model / 'checkpoint.pt').exists():
        trainer = LanguageModelTrainer.load_checkpoint(model / 'checkpoint.pt', corpus)
        epochs = 5
    else:
        language_model = LanguageModel(dictionary,
                                       forward,
                                       hidden_size=hidden_size,
                                       nlayers=1)
        trainer = LanguageModelTrainer(language_model, corpus)
        epochs = 50

    trainer.train(model,
                  sequence_length=10,
                  mini_batch_size=batch_size,
                  max_epochs=trainer.epoch+epochs,
                  checkpoint=True)


def make_embeddings(model_path: Path) -> TokenEmbeddings:
    forward = model_path / "forward" / 'best-lm.pt'
    backward = model_path / "backward" / 'best-lm.pt'
    embedding_types: List[TokenEmbeddings] = [
        FlairEmbeddings(forward),
        FlairEmbeddings(backward),
    ]
    embeddings: StackedEmbeddings = StackedEmbeddings(embeddings=embedding_types)
    return embeddings
