#!/usr/bin/env python
import logging
import sys
import shutil

from pathlib import Path

import flair
import sklearn.model_selection
import torch

from flair.datasets import ColumnCorpus
from flair.models import SequenceTagger
from flair.trainers import ModelTrainer

from n_utils import make_embeddings, make_vocab, train_lm_model

_LOGGER = logging.getLogger(__name__)

_MODEL = Path(__file__).parent / 'tag_model'
_MODEL.mkdir(exist_ok=True)

_SPLITS_DIR = Path(__file__).parent / 'data' / 'splits'

_TAGS_COLUMN_SEPARATOR = '\tXXX\t'


def make_corpus(source_files=None):
    with open(_MODEL / 'split-corpus.txt', 'w') as corpus:
        for f in _SPLITS_DIR.iterdir():
            with open(f) as labeled:
                data = labeled.read()
                samples = data.split("\n\n")
            for sample in samples:
                for l in sample.split('\n'):
                    if not l:
                        continue
                    word, *_ = l.split(_TAGS_COLUMN_SEPARATOR, 1)
                    print(word, file=corpus)
        if source_files is not None:
            for f in source_files:
                with open(f) as raw:
                    data = raw.read()
                    samples = data.split("\n\n")
                for sample in samples:
                    for l in sample.split('\n'):
                        if not l:
                            continue
                        print(l, file=corpus)
    (_MODEL / 'split-corpus/train').mkdir(parents=True, exist_ok=True)
    shutil.copyfile(_MODEL / 'split-corpus.txt', _MODEL / 'split-corpus/train/train.txt')
    shutil.copyfile(_MODEL / 'split-corpus.txt', _MODEL / 'split-corpus/test.txt')
    shutil.copyfile(_MODEL / 'split-corpus.txt', _MODEL / 'split-corpus/valid.txt')
    return _MODEL / 'split-corpus'


def make_split_corpus(source_files=None):
    result = _MODEL / 'split-corpus'
    result.mkdir(parents=True, exist_ok=True)

    samples = []
    for f in _SPLITS_DIR.iterdir():
        with open(f) as labeled:
            samples.extend(s
                           for s in labeled.read().split("\n\n")
                           if s)
    if source_files is not None:
        for f in source_files:
            with open(f) as raw:
                sample = []
                for l in raw:
                    if not l:
                        continue
                    sample.append(l + _TAGS_COLUMN_SEPARATOR + 'O')
                samples.append('\n'.join(sample))

    if len(samples) > 10:
        train, tv = sklearn.model_selection.train_test_split(samples, test_size=0.1)
    else:
        train, tv = samples, samples

    if len(tv) > 2:
        test, valid = sklearn.model_selection.train_test_split(tv, test_size=0.5)
    else:
        test, valid = tv, tv

    _LOGGER.info("Train: %d lines", len(train))
    _LOGGER.info("Test: %d lines", len(test))
    _LOGGER.info("Validation: %d lines", len(valid))

    with open(result / 'train.txt', 'w') as out:
        for text in train:
            print(text, file=out)
            print(file=out)
    with open(result / "test.txt", "w") as out:
        for text in test:
            print(text, file=out)
            print(file=out)
    with open(result / "dev.txt", "w") as out:
        for text in valid:
            print(text, file=out)
            print(file=out)

    return result


def train_split_model(corpus_path, word_embeddings, model, batch_size=128, epochs=150):
    corpus = ColumnCorpus(corpus_path, column_format={0: 'text', 1: 'ner'},
                          column_delimiter=_TAGS_COLUMN_SEPARATOR)
    tag_dictionary = corpus.make_label_dictionary(label_type="ner")

    if (model / 'final-model.pt').exists():
        tagger = SequenceTagger.load(model / 'final-model.pt')
        params = {"max_epochs": 50}
    else:
        tagger: SequenceTagger = SequenceTagger(hidden_size=64,
                                                embeddings=word_embeddings,
                                                tag_dictionary=tag_dictionary,
                                                tag_type="ner",
                                                tag_format="BIO",
                                                use_crf=True)
        params = {"max_epochs": 150}
    trainer = ModelTrainer(tagger, corpus)
    trainer.train(model,
                  learning_rate=0.1,
                  mini_batch_size=batch_size,
                  anneal_factor=0.5,
                  patience=5,
                  **params)


def main(source_files):
    corpus = make_corpus(source_files)
    mappings, changed = make_vocab(_MODEL, 'split-corpus')
    if changed:
        if (_MODEL / 'forward').exists():
            shutil.rmtree(_MODEL / 'forward')
        if (_MODEL / 'backward').exists():
            shutil.rmtree(_MODEL / 'backward')
        if (_MODEL / 'tag').exists():
            shutil.rmtree(_MODEL / 'tag')

    train_lm_model(corpus, mappings, _MODEL / 'forward', forward=True)
    train_lm_model(corpus, mappings, _MODEL / 'backward', forward=False)

    split_corpus = make_split_corpus(source_files)
    embeddings = make_embeddings(_MODEL)
    train_split_model(split_corpus, embeddings, _MODEL / 'split')


if __name__ == '__main__':
    flair.device = torch.device('cuda:0')
    log_format = "%(asctime)-8s:%(levelname)5s:%(name)s: %(message)s"
    logging.basicConfig(stream=sys.stderr,
                        level=logging.DEBUG,
                        format=log_format,
                        datefmt="%H:%M:%S")

    main(sys.argv[1:])
